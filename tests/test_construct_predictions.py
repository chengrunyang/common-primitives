import copy
import json
import os
import unittest

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base

from common_primitives import dataset_to_dataframe, construct_predictions, extract_columns_semantic_types, utils


def convert_metadata(metadata):
    return json.loads(json.dumps(metadata, cls=d3m_utils.JsonEncoder))


class ConstructPredictionsPrimitiveTestCase(unittest.TestCase):
    # TODO: Make this part of metadata API.
    #       Something like setting a semantic type for given columns.
    def _mark_all_targets(self, dataset, targets):
        for target in targets:
            dataset.metadata = dataset.metadata.add_semantic_type((target['resource_id'], metadata_base.ALL_ELEMENTS, target['column_index']), 'https://metadata.datadrivendiscovery.org/types/Target')
            dataset.metadata = dataset.metadata.add_semantic_type((target['resource_id'], metadata_base.ALL_ELEMENTS, target['column_index']), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')

    def _get_iris_dataframe(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        self._mark_all_targets(dataset, [{'resource_id': '0', 'column_index': 5}])

        hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())

        call_metadata = primitive.produce(inputs=dataset)

        dataframe = call_metadata.value

        return dataframe

    def test_correct_order(self):
        dataframe = self._get_iris_dataframe()

        hyperparams_class = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        # We extract both the primary index and targets. So it is in the output format already.
        primitive = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive(hyperparams=hyperparams_class.defaults().replace({'semantic_types': ('https://metadata.datadrivendiscovery.org/types/PrimaryKey', 'https://metadata.datadrivendiscovery.org/types/Target',)}))

        call_metadata = primitive.produce(inputs=dataframe)

        targets = call_metadata.value

        # We pretend these are our predictions.
        targets.metadata = targets.metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        targets.metadata = targets.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget')

        # We switch columns around.
        targets = utils.select_columns(targets, [1, 0])

        hyperparams_class = construct_predictions.ConstructPredictionsPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        construct_primitive = construct_predictions.ConstructPredictionsPrimitive(hyperparams=hyperparams_class.defaults())

        call_metadata = construct_primitive.produce(inputs=targets, reference=dataframe)

        dataframe = call_metadata.value

        self.assertIs(dataframe, dataframe.metadata.for_value)

        self.assertEqual(list(dataframe.columns), ['d3mIndex', 'species'])

        self._test_metadata(dataframe.metadata)

    def test_correct_order_can_accept(self):
        dataframe = self._get_iris_dataframe()

        hyperparams_class = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        # We extract both the primary index and targets. So it is in the output format already.
        primitive = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive(hyperparams=hyperparams_class.defaults().replace({'semantic_types': ('https://metadata.datadrivendiscovery.org/types/PrimaryKey', 'https://metadata.datadrivendiscovery.org/types/Target',)}))

        call_metadata = primitive.produce(inputs=dataframe)

        targets = call_metadata.value

        # We pretend these are our predictions.
        targets.metadata = targets.metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        targets.metadata = targets.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget')

        # We switch columns around.
        targets = utils.select_columns(targets, [1, 0])

        targets_metadata = targets.metadata.set_for_value(None)
        dataframe_metadata = dataframe.metadata.set_for_value(None)

        hyperparams_class = construct_predictions.ConstructPredictionsPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        metadata = construct_predictions.ConstructPredictionsPrimitive.can_accept(method_name='produce', arguments={'inputs': targets_metadata, 'reference': dataframe_metadata}, hyperparams=hyperparams_class.defaults())

        self.assertTrue(metadata)

        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length'], 2)

        self._test_metadata(metadata)

    def test_all_columns(self):
        dataframe = self._get_iris_dataframe()

        # We use all columns. Output has to be just index and targets.
        targets = copy.copy(dataframe)
        # Just to be sure.
        targets.metadata = dataframe.metadata.set_for_value(targets)

        # We pretend these are our predictions.
        targets.metadata = targets.metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        targets.metadata = targets.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget')

        hyperparams_class = construct_predictions.ConstructPredictionsPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        construct_primitive = construct_predictions.ConstructPredictionsPrimitive(hyperparams=hyperparams_class.defaults())

        call_metadata = construct_primitive.produce(inputs=targets, reference=dataframe)

        dataframe = call_metadata.value

        self.assertIs(dataframe, dataframe.metadata.for_value)

        self.assertEqual(list(dataframe.columns), ['d3mIndex', 'species'])

        self._test_metadata(dataframe.metadata)

    def test_all_columns_can_accept(self):
        dataframe = self._get_iris_dataframe()

        # We use all columns. Output has to be just index and targets.
        targets = copy.copy(dataframe)
        # Just to be sure.
        targets.metadata = dataframe.metadata.set_for_value(targets)

        # We pretend these are our predictions.
        targets.metadata = targets.metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        targets.metadata = targets.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget')

        targets_metadata = targets.metadata.set_for_value(None)
        dataframe_metadata = dataframe.metadata.set_for_value(None)

        hyperparams_class = construct_predictions.ConstructPredictionsPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        metadata = construct_predictions.ConstructPredictionsPrimitive.can_accept(method_name='produce', arguments={'inputs': targets_metadata, 'reference': dataframe_metadata}, hyperparams=hyperparams_class.defaults())

        self.assertTrue(metadata)

        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length'], 2)

        self._test_metadata(metadata)

    def test_missing_index(self):
        dataframe = self._get_iris_dataframe()

        # We just use all columns.
        targets = copy.copy(dataframe)
        # Just to be sure.
        targets.metadata = dataframe.metadata.set_for_value(targets)

        # We pretend these are our predictions.
        targets.metadata = targets.metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        targets.metadata = targets.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget')

        # Remove primary index. This one has to be reconstructed.
        targets = utils.remove_columns(targets, [0])

        hyperparams_class = construct_predictions.ConstructPredictionsPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        construct_primitive = construct_predictions.ConstructPredictionsPrimitive(hyperparams=hyperparams_class.defaults())

        call_metadata = construct_primitive.produce(inputs=targets, reference=dataframe)

        dataframe = call_metadata.value

        self.assertIs(dataframe, dataframe.metadata.for_value)

        self.assertEqual(list(dataframe.columns), ['d3mIndex', 'species'])

        self._test_metadata(dataframe.metadata)

    def test_missing_index_can_accept(self):
        dataframe = self._get_iris_dataframe()

        # We just use all columns.
        targets = copy.copy(dataframe)
        # Just to be sure.
        targets.metadata = dataframe.metadata.set_for_value(targets)

        # We pretend these are our predictions.
        targets.metadata = targets.metadata.remove_semantic_type((metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')
        targets.metadata = targets.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 5), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget')

        # Remove primary index. This one has to be reconstructed.
        targets = utils.remove_columns(targets, [0])

        targets_metadata = targets.metadata.set_for_value(None)
        dataframe_metadata = dataframe.metadata.set_for_value(None)

        hyperparams_class = construct_predictions.ConstructPredictionsPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        metadata = construct_predictions.ConstructPredictionsPrimitive.can_accept(method_name='produce', arguments={'inputs': targets_metadata, 'reference': dataframe_metadata}, hyperparams=hyperparams_class.defaults())

        self.assertTrue(metadata)

        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length'], 2)

        self._test_metadata(metadata)

    def test_just_targets_no_metadata(self):
        dataframe = self._get_iris_dataframe()

        hyperparams_class = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        # We extract just targets.
        primitive = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive(hyperparams=hyperparams_class.defaults().replace({'semantic_types': ('https://metadata.datadrivendiscovery.org/types/Target',)}))

        call_metadata = primitive.produce(inputs=dataframe)

        targets = call_metadata.value

        # Remove all metadata.
        targets.metadata = targets.metadata.clear(for_value=targets)

        hyperparams_class = construct_predictions.ConstructPredictionsPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        construct_primitive = construct_predictions.ConstructPredictionsPrimitive(hyperparams=hyperparams_class.defaults())

        call_metadata = construct_primitive.produce(inputs=targets, reference=dataframe)

        dataframe = call_metadata.value

        self.assertIs(dataframe, dataframe.metadata.for_value)

        self.assertEqual(list(dataframe.columns), ['d3mIndex', 'species'])

        self._test_metadata(dataframe.metadata, True)

    def test_just_targets_no_metadata_can_accept(self):
        dataframe = self._get_iris_dataframe()

        hyperparams_class = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        # We extract just targets.
        primitive = extract_columns_semantic_types.ExtractColumnsBySemanticTypesPrimitive(hyperparams=hyperparams_class.defaults().replace({'semantic_types': ('https://metadata.datadrivendiscovery.org/types/Target',)}))

        call_metadata = primitive.produce(inputs=dataframe)

        targets = call_metadata.value

        # Remove all metadata.
        targets.metadata = targets.metadata.clear(for_value=targets)

        targets_metadata = targets.metadata.set_for_value(None)
        dataframe_metadata = dataframe.metadata.set_for_value(None)

        hyperparams_class = construct_predictions.ConstructPredictionsPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        metadata = construct_predictions.ConstructPredictionsPrimitive.can_accept(method_name='produce', arguments={'inputs': targets_metadata, 'reference': dataframe_metadata}, hyperparams=hyperparams_class.defaults())

        self.assertTrue(metadata)

        self.assertEqual(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length'], 2)

        self._test_metadata(metadata, True)

    def _test_metadata(self, metadata, no_metadata=False):
        self.maxDiff = None

        self.assertEqual(convert_metadata(metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 150,
            }
        })

        self.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS,))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 2,
            }
        })

        self.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        if no_metadata:
            self.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS, 1))), {
                'name': 'species',
                'structural_type': 'str',
            })

        else:
            self.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS, 1))), {
                'name': 'species',
                'structural_type': 'str',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/CategoricalData',
                    'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
                    'https://metadata.datadrivendiscovery.org/types/Target',
                    'https://metadata.datadrivendiscovery.org/types/PredictedTarget',
                ],
            })


if __name__ == '__main__':
    unittest.main()
