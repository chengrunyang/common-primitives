import unittest
import os
from distutils import version

import d3m
from d3m import container

from common_primitives import dataset_to_dataframe, csv_reader


class CSVReaderPrimitiveTestCase(unittest.TestCase):
    @unittest.skipUnless(version.LooseVersion(d3m.__version__) > version.LooseVersion('2018.7.10'), "Fixed in newer versions of core package.")
    def test_basic(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'timeseries_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        dataframe_hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        dataframe_primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=dataframe_hyperparams_class.defaults().replace({'dataframe_resource': '0'}))
        dataframe = dataframe_primitive.produce(inputs=dataset).value

        csv_hyperparams_class = csv_reader.CSVReaderPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        csv_primitive = csv_reader.CSVReaderPrimitive(hyperparams=csv_hyperparams_class.defaults().replace({'return_result': 'replace'}))
        tables = csv_primitive.produce(inputs=dataframe).value

        self.assertEqual(tables.shape, (1, 1))

        self._test_metadata(tables.metadata)

    @unittest.skipUnless(version.LooseVersion(d3m.__version__) > version.LooseVersion('2018.7.10'), "Fixed in newer versions of core package.")
    def test_can_accept(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'timeseries_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        dataset_metadata = dataset.metadata.set_for_value(None)

        dataframe_hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        dataframe_metadata = dataset_to_dataframe.DatasetToDataFramePrimitive.can_accept(method_name='produce', arguments={'inputs': dataset_metadata}, hyperparams=dataframe_hyperparams_class.defaults().replace({'dataframe_resource': '0'}))

        csv_hyperparams_class = csv_reader.CSVReaderPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        tables_metadata = csv_reader.CSVReaderPrimitive.can_accept(method_name='produce', arguments={'inputs': dataframe_metadata}, hyperparams=csv_hyperparams_class.defaults().replace({'return_result': 'replace'}))

        self._test_metadata(tables_metadata)

    def _test_metadata(self, metadata):
        self.assertEqual(metadata.query_column(0)['structural_type'], container.DataFrame)
        self.assertEqual(metadata.query_column(0)['semantic_types'], ('https://metadata.datadrivendiscovery.org/types/PrimaryKey', 'https://metadata.datadrivendiscovery.org/types/Timeseries', 'https://metadata.datadrivendiscovery.org/types/Table'))

        self.assertEqual(metadata.query_column(0, at=(0, 0)), {
            'structural_type': str,
            'name': 'Date',
            'semantic_types': (
                'https://metadata.datadrivendiscovery.org/types/Time',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            )
        })
        self.assertEqual(metadata.query_column(1, at=(0, 0)), {
            'structural_type': str,
            'name': 'Close',
            'semantic_types': (
               'http://schema.org/Float',
               'https://metadata.datadrivendiscovery.org/types/Attribute',
            )
        })


if __name__ == '__main__':
    unittest.main()
